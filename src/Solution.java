/*1. Нужно создать нерегулярный массив, подобный table:
int table[ ] [ ] = new int[ 4] [ ];
table[0] = new int[7];
table[1] = new int[3];
table[2] = new int[5];
table[3] = new int[1];
Количество строк вводится, а количество элементов в каждой строке
задается случайным образом в диапазоне от 1 до 10.
2. Заполнить его какими-нибудь элементами.
3. Вывести массив красиво (чтоб он выглядел как массив, а не строка
или столбец ).
4. Необходимо переставить строки массива, чтобы их размер возрастал
(или убывал).
5. Вывести получившийся массив.
Для массива table результат будет выглядеть так:
table[0] = new int[1];
table[1] = new int[3];
table[2] = new int[5];
table[3] = new int[7];

 */

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        int[][] table = new int[4][];
        Scanner sc = new Scanner(System.in);
        if(sc.hasNextInt()) {
            table[0] = new int[sc.nextInt()];
            table[1] = new int[sc.nextInt()];
            table[2] = new int[sc.nextInt()];
            table[3] = new int[sc.nextInt()];
        }

        for (int i = 0; i < table.length; i++) {
            for(int j = 0; j < table[i].length; j++) {
                table[i][j] = (int)(Math.random()*10);
                System.out.print(table[i][j] + " ");
                if(j==table[i].length-1) {
                    System.out.println(" ");
                }
            }
        }

        Solution.sortingArray(table);
    }
    private static void sortingArray(int[][] array) {

        for (int i = 0; i < array.length-1; i++) {
            for (int j = 1; j < array.length; j++) {
                if(array[j - 1].length > array[j].length) {
                    int[] temp = array[j - 1];
                    array[j - 1] = array[j] ;
                    array[j] = temp;


                }
            }
        }
        System.out.println();
        for (int[] ints : array) {
            for (int j = 0; j < ints.length; j++) {
                System.out.print(ints[j] + " ");
                if (j == ints.length - 1) {
                    System.out.println(" ");
                }
            }
        }
    }
}
